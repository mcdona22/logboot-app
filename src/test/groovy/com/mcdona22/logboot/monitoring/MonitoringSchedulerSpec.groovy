package com.mcdona22.logboot.monitoring

import com.mcdona22.logboot.monitoring.monitorables.IMonitorable
import spock.lang.Specification

class MonitoringSchedulerSpec extends Specification{
    MonitoringScheduler scheduler
    IMonitorable monitor = Mock(IMonitorable)

    def setup(){
        scheduler = new MonitoringScheduler()
    }

    def "the monitorable should raise an alert when the event is detected"(){
        setup:
            monitor.getEventRaised() >> false
        when:
            scheduler.runMonitorable(monitor)
        then:
            1 * monitor.conditionExists() >> true
            1 * monitor.sendEventRaiseMessage()
            0 * monitor.sendEventClearMessage()

    }

    def "the monitorable should not raise an event when the event has already been detected"(){
        setup:
            monitor.getEventRaised() >> true
        when:
            scheduler.runMonitorable(monitor)
        then:
            1 * monitor.conditionExists() >> true
            0 * monitor.sendEventRaiseMessage()
            0 * monitor.sendEventClearMessage()

    }

    def "the monitorable should raise a CLEAR event if an event is detected and the condition recovers"(){
        setup:
            monitor.getEventRaised() >> true
            1 * monitor.conditionExists() >> false
        when:
            scheduler.runMonitorable(monitor)
        then:
            1 * monitor.sendEventClearMessage()
            0 * monitor.sendEventRaiseMessage()
    }

    def "the monitorable should raise no event if the condition is OK and there is no event currently raised"() {
        setup:
            monitor.getEventRaised() >> false
            1 * monitor.conditionExists() >> false

        when:
            scheduler.runMonitorable(monitor)
        then:
            0 * monitor.sendEventClearMessage()
            0 * monitor.sendEventRaiseMessage()
    }


}
