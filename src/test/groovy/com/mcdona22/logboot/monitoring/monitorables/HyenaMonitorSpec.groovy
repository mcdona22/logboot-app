package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.monitoring.GalmLogger
import spock.lang.Specification

class HyenaMonitorSpec extends Specification{
    HyenaMonitor monitor
    GalmLogger galmMock
    String expectedMessage

    def setup(){
        monitor = new HyenaMonitor()
        galmMock = Mock()
        monitor.galmLogger = galmMock
        monitor.predators.hyenas = 12
        expectedMessage = "Whoa!  There are ${monitor.predators.hyenas}. Thats dangerous"

    }

    def "when raising and clearing an event the message portion of the GALM message should be the same "(){
        when:
            this.monitor.sendEventRaiseMessage()
            println this.monitor.message
            this.monitor.sendEventClearMessage()

        then:
            1 * this.galmMock.logMessage(_, _, _, expectedMessage)
            1 * this.galmMock.clearEvent(_, expectedMessage)
            ! monitor.message
    }

    def "between raising and clearing an event the message should remain the same even if the hyena count changes"(){
        when:
            monitor.sendEventRaiseMessage()
            monitor.predators.hyenas = 2000
            monitor.sendEventClearMessage()
        then:
            1 * galmMock.logMessage(_, _, _, expectedMessage)
            1 * galmMock.clearEvent(_, expectedMessage)
            ! monitor.message
    }
}
