package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.Application
import com.mcdona22.logboot.monitoring.monitorables.HyenaMonitor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration( classes = [Application])
class HyenaMonitoringIntegrationSpec extends  Specification{\
    @Autowired HyenaMonitor monitor

    def "component should be wired correctly"(){
        expect:
            monitor
            monitor.galmLogger
    }
}
