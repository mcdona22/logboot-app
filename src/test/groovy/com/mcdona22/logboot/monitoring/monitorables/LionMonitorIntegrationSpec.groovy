package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.Application
import com.mcdona22.logboot.monitoring.EventType
import com.mcdona22.logboot.monitoring.Severity
import com.mcdona22.logboot.monitoring.Stage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import spock.lang.Specification


@ContextConfiguration( classes = [Application])
class LionMonitorIntegrationSpec extends Specification{

    @Autowired LionMonitor monitor

    def "component should be wired correctly"(){
        expect:
            monitor
            monitor.galmLogger
            //monitor.galmLogger.logMessage(EventType.FUNCTIONAL, Severity.CRITICAL, Stage.TEST, "integration test here")
    }
}
