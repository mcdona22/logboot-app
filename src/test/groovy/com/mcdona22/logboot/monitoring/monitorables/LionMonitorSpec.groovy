package com.mcdona22.logboot.monitoring.monitorables

import spock.lang.Specification

class LionMonitorSpec extends Specification{

     def "a new monitor should not be in an eventing state on creation"(){
        expect:
        ! new LionMonitor().getEventRaised()
    }
}
