package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.monitoring.EventType
import com.mcdona22.logboot.monitoring.GalmLogger
import com.mcdona22.logboot.monitoring.Severity
import com.mcdona22.logboot.monitoring.Stage
import spock.lang.Specification

class XfbFileMonitorSpec extends Specification{
    String xfbErrDirName = "target/test/xfb/errors"
    String xfbDirName = "target/test/xfb/transfer"
    File errorDir
    File txDir
    int testTimeToLive = 1000000

    XfbFileMonitor monitor
    GalmLogger logger = Mock()

    def setup(){
        destroyTestDir()
        monitor = new XfbFileMonitor()
        monitor.logger = logger
        monitor.errorFileLocaton = errorDir
        monitor.xfbTransferDir = txDir
        monitor.timeTolive = testTimeToLive
    }


    void destroyTestDir(){
        errorDir = new File(xfbErrDirName)
        errorDir.deleteDir()
        errorDir.mkdirs()

        txDir = new File(xfbDirName)
        txDir.deleteDir()
        txDir.mkdirs()
    }

    def "when raising and clearing an event the message portion of the GALM message should be the same"(){
        setup:
            new File("$xfbErrDirName/temp.txt").createNewFile()
            String expectedMessage = "There are files that have failed XFB transfer"
        when:
            monitor.sendEventRaiseMessage()
            monitor.sendEventClearMessage()

        then:
            1 * logger.logMessage(_ as EventType, _ as Severity, _ as Stage, expectedMessage)
            1 * logger.clearEvent(_ as Stage, expectedMessage)
            monitor.message == ""
    }



    def "if there are files in the error directory then we are in a alert condition"(){
        setup:
            new File("$xfbErrDirName/temp.txt").createNewFile()
        expect:
            monitor.conditionExists()
    }

    def "if there are files that have lived too long then the alert condition exists"(){
        setup:
            long timestamp = new Date().time
            File goodFile = new File("$xfbDirName/I-can-be-here.txt")
            goodFile.createNewFile()
            File testFile = new File("$xfbDirName/sdgqdfrgq.txt")
            testFile.createNewFile()
            println "the tine is ${testFile.lastModified()}"
            testFile.setLastModified (timestamp - testTimeToLive - 5)
            println "the tine is ${testFile.lastModified()}"
        expect:
            monitor.conditionExists()
    }

    def "if there are 0 files in error and no files that exceed their duration then the condition does not exist"(){
        setup:
            File testFile = new File("$xfbDirName/I-can-be-here.txt")
            testFile.createNewFile()
        expect:
            ! monitor.conditionExists()
    }

    def "sending the message invokes the monitors logger and sets state to recognise it is in alert"(){
        when:
            monitor.sendEventRaiseMessage()
        then:
            1 * logger.logMessage(_ as EventType, _ as Severity, _ as Stage, _ as String)
            monitor.eventRaised
    }

    def "sending the clear message should invoke the logger and reset the incident state"(){
        when:
            monitor.sendEventClearMessage()
            monitor.conditionExists = true

        then:
            1 * logger.clearEvent(_ as Stage, _ as String)
            //! monitor.conditionExists
    }

}
