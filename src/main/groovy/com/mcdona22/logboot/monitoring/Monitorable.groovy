package com.mcdona22.logboot.monitoring

import com.mcdona22.logboot.monitoring.monitorables.IMonitorable
import org.slf4j.Logger
import org.slf4j.LoggerFactory


abstract class Monitorable {
    final  Logger log = LoggerFactory.getLogger(this.class)
    IMonitorable watcher
    GalmLogger galmLogger
    boolean eventDetected

    void execute(){
        if( watcher.conditionExists()){
            ! eventDetected && (eventDetected = true) && galmLogger.logMessage(EventType.FUNCTIONAL, Severity.CRITICAL, Stage.TEST, watcher.message())

        } else {
            if(eventDetected) {
                eventDetected = false
                galmLogger.clearEvent()
            }
        }
    }

}
