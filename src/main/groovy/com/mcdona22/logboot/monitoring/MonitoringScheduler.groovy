package com.mcdona22.logboot.monitoring

import com.mcdona22.logboot.monitoring.monitorables.IMonitorable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.BeansException
import org.springframework.beans.factory.BeanFactoryUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

@EnableScheduling
@Service("MonitoringScheduler")
class MonitoringScheduler implements ApplicationContextAware{
    final static Logger LOG = LoggerFactory.getLogger(MonitoringScheduler)
    Timer timer = new Timer("App-Monitoring-Timer")

    ApplicationContext context
    Map<String, Monitorable> monitorables
    @Autowired GalmLogger heartbeatLogger

    @Override
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context  = applicationContext
    }

    @PostConstruct
    void initialiseBeanList(){
        LOG.info "Discovering beans"

        monitorables  = BeanFactoryUtils.beansOfTypeIncludingAncestors(context, IMonitorable);

        monitorables.each{name, bean -> LOG.info "found $name"}

        LOG.info "beans discovery complete"

    }

    @Scheduled(initialDelay = 2000L, fixedRate = 3000L)
    void runMonitors(){
        LOG.info "running the monitors"
        monitorables.each{String name, IMonitorable monitorableBean ->
            LOG.debug "running $name"
            runMonitorable(monitorableBean)
        }
    }

    @Scheduled(initialDelay= 2000L, fixedDelay = 30000L)
    void heartbeat(){
        //heartbeatLogger.logMessage(EventType.HEARTBEAT, Severity.HARMLESS, Stage.TEST, "T15 appl_pulse")
        heartbeatLogger.logHeartbeat( "T15")
    }

    void runMonitorable(IMonitorable monitorable){

        if(monitorable.conditionExists()) {
            LOG.debug "The condition exists"
            if (! monitorable.getEventRaised()){
                LOG.debug "no event has been raised"
                monitorable.sendEventRaiseMessage()
            }
        } else {
            LOG.debug "the alarm condition does not exist"
            if( monitorable.getEventRaised()) {
                monitorable.sendEventClearMessage()
            }
        }
    }
}
