package com.mcdona22.logboot.monitoring

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

enum EventType {FUNCTIONAL, TECHNICAL, CLEARING, HEARTBEAT}
enum Severity {FATAL, CRITICAL, WARNING, HARMLESS}
enum Stage {TEST, ACCEPT, PROD}

@Component
class GalmLogger {
    // Specifics of your server. This should come from some config fi.e.
    String host = "mcmacmac.local"
    String comp = "mac-test-comp"
    String subComp = comp

    final static Logger LOG = LoggerFactory.getLogger(GalmLogger)

    //TODO - how do we get an event id with which to reconclie raising and clearing events
    void logMessage(EventType eventType, Severity severity, Stage stage, String message){
        String timestamp = new Date().format("yyyy-MM-dd HH:mm:ss")
        LOG.info "$timestamp $host $comp $subComp $eventType $severity $stage $message"

    }

    void clearEvent(Stage stage, String message){
        String timestamp = new Date().format("yyyy-MM-dd HH:mm:ss")
        LOG.info "$timestamp $host $comp $subComp $EventType.CLEARING $Severity.HARMLESS $stage $message"

    }

    void logHeartbeat(String interval){
        // Add description of heartbeat
        String timestamp = new Date().format("yyyy-MM-dd HH:mm:ss")

        LOG.info "$timestamp $EventType.HEARTBEAT $Severity.HARMLESS $Stage.TEST $interval appl_pulse"
    }
}
