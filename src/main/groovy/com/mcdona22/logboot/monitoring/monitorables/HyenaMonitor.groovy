package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.monitoring.EventType
import com.mcdona22.logboot.monitoring.GalmLogger
import com.mcdona22.logboot.monitoring.Severity
import com.mcdona22.logboot.monitoring.Stage
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class HyenaMonitor implements IMonitorable{
    final static Logger LOG = LoggerFactory.getLogger(LionMonitor)
    @Autowired GalmLogger galmLogger

    File animals = new File("animals.js")
    Map predators = [:]
    boolean tooManyHyenas
    final int MAX_HYENAS = 4
    String message = ""

    @Override
    boolean conditionExists() {
        predators = readAnimalsFromFile(animals)

        return predators.hyenas > MAX_HYENAS
    }

    @Override
    boolean getEventRaised() {
        return tooManyHyenas
    }

    
    @Override
    void sendEventRaiseMessage() {
        message = "Whoa!  There are ${predators.hyenas}. Thats dangerous"
        tooManyHyenas = true
        galmLogger.logMessage(EventType.FUNCTIONAL, Severity.CRITICAL, Stage.TEST, message )

    }

    @Override
    void sendEventClearMessage() {
        tooManyHyenas = false
        galmLogger.clearEvent(Stage.TEST, message)
        message = ""
    }

    Map readAnimalsFromFile(f){
        return new JsonSlurper().parse(f)
    }
}
