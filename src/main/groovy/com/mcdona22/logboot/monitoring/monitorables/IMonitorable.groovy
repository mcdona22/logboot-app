package com.mcdona22.logboot.monitoring.monitorables

interface IMonitorable {

    boolean conditionExists()
    boolean getEventRaised()
    void sendEventRaiseMessage()
    void sendEventClearMessage()
}
