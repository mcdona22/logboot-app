package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.monitoring.EventType
import com.mcdona22.logboot.monitoring.GalmLogger
import com.mcdona22.logboot.monitoring.Severity
import com.mcdona22.logboot.monitoring.Stage
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class XfbFileMonitor implements IMonitorable{

    final static Logger LOG = LoggerFactory.getLogger(XfbFileMonitor)
    File errorFileLocaton = new File("target/test/xfb/errors")
    File xfbTransferDir = new File("target/test/xfb/transfer")
    int timeTolive
    boolean conditionExists = false

    String message = ""

    @Autowired GalmLogger logger


    @Override
    boolean conditionExists() {
        // Check if there are files in the error folder.
        if(errorFileLocaton.list().size() > 0) {
            LOG.error("There are files in $errorFileLocaton idicating and XFB failure")
            return true
        }

        // Check if the files that are waithing to be transported are not to old
        boolean flag = false
        List files = xfbTransferDir.listFiles()
        Long oldestFilePossible = new Date().time - timeTolive

        files.each {File file ->
            if (file.lastModified() < oldestFilePossible) {
                LOG.error "$file is too old"
                flag = true
            }

        }
        return flag

    }

    @Override
    boolean getEventRaised() {
        return conditionExists
    }

    @Override
    void sendEventRaiseMessage() {
        conditionExists = true
        message = "There are files that have failed XFB transfer"
        logger.logMessage(EventType.FUNCTIONAL,Severity.CRITICAL,Stage.PROD, message)
    }

    @Override
    void sendEventClearMessage() {
        conditionExists = false
        logger.clearEvent(Stage.PROD, message)
        message = ""
    }
}
