package com.mcdona22.logboot.monitoring.monitorables

import com.mcdona22.logboot.monitoring.EventType
import com.mcdona22.logboot.monitoring.GalmLogger
import com.mcdona22.logboot.monitoring.Severity
import com.mcdona22.logboot.monitoring.Stage
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class LionMonitor implements IMonitorable{
    final static Logger LOG = LoggerFactory.getLogger(LionMonitor)
    File animals = new File("animals.js")
    boolean lionsAhoy
    @Autowired GalmLogger galmLogger


    boolean getEventRaised(){
        return lionsAhoy
    }

    @Override
    boolean conditionExists() {
        Map animals = readAnimalsFromFile(animals)
        LOG.debug animals.toString()

        return  animals?.lions  // if there are any sound the bell
    }

    @Override
    void sendEventRaiseMessage() {
        LOG.warn "Monitoring event raised"
        lionsAhoy = true
        galmLogger.logMessage(EventType.FUNCTIONAL, Severity.CRITICAL, Stage.TEST, "OMG - Lions!!!!" )

    }

    @Override
    void sendEventClearMessage() {
        LOG.info "Monitoring Event Cleared"
        lionsAhoy = false
        galmLogger.clearEvent(Stage.TEST, "OK - all the lions are gone now")

    }


    Map readAnimalsFromFile(f){
        return new JsonSlurper().parse(f)
    }
}
