package com.mcdona22.logboot

import com.mcdona22.logboot.monitoring.EventType
import com.mcdona22.logboot.monitoring.GalmLogger
import com.mcdona22.logboot.monitoring.Severity
import com.mcdona22.logboot.monitoring.Stage
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.ComponentScan

@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
class Application {
    final static Logger LOG = LoggerFactory.getLogger(Application)

    static main(args){
        SpringApplication application = new SpringApplication(com.mcdona22.logboot.Application.class)
        application.showBanner = false

        ApplicationContext context =  application.run args

        LOG.info "application is up and running"


    }
}
